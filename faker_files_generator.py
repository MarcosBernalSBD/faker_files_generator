#! /usr/bin/env python2
"""
Reads from the configuration file passed as parameter and creates a faker data file.
"""
import os, sys, argparse, json
from faker import Faker
from decimal import Decimal
from pyspark import SparkContext
from pyspark.sql.session import SparkSession


def createValuesFromDict(dict_object, faker_factory):
    return {str(key): createValue(faker_factory, dict_object[key]) for key in dict_object.keys()}


def createValue(faker_factory, directive):
    if isinstance(directive, (list,)):
        return [createValue(faker_factory=faker_factory, directive=element) for element in directive]
    elif isinstance(directive, (dict,)):
        return createValuesFromDict(dict_object=directive, faker_factory=faker_factory)
    elif isinstance(directive, str) or isinstance(directive, unicode):
        directive = directive.encode("utf-8", "ignore")
        command = directive if directive[-1] == ')' else directive + "()"
        try:
            value = eval('faker_factory.'+command)
            value = float(value) if isinstance(value, Decimal) else value
        except (AttributeError, SyntaxError):
            value = directive  # If command is not available in faker library, the command name will be used instead
        return value
    else:
        return directive


def generateDataFrameFromDictionaryList(dict_list, destinyPath, spark, sparkContext):
    dict_rdd = sparkContext.parallelize(dict_list)
    dict_df = spark.read.json(dict_rdd)
    dict_df.coalesce(1).write.mode("overwrite").save(destinyPath)


def main():
    parser = argparse.ArgumentParser(description=__doc__)

    required = parser.add_argument_group('required arguments')
    optional = parser.add_argument_group('optional arguments')
    required.add_argument('-i', action='store', dest='configFile', help="configuration file with the required fields", required=True)
    required.add_argument('-o', action='store', dest='outputPath', help="path where the output file will be stored", required=True)
    optional.add_argument('-n', action='store', dest='nrows', type=int, help="number of rows in the resulting dataframe", default=1)
    optional.add_argument('-ov', action='store', dest='overwrite', type=bool, help="disable the path safety check. Change to True", default=False)

    args = parser.parse_args()

    if not os.path.isfile(args.configFile):
        print("File '" + args.configFile + "' can not be find")
        sys.exit(1)
    try:
        json_value = json.loads(open(args.configFile, 'r').read())
    except ValueError:
        print("Json File '" + args.configFile + "' was not properly formed")
        sys.exit(1)

    if os.access(args.outputPath, os.W_OK) and not args.overwrite:
        print("The output path: '" + args.outputPath + "' can not be used to write a file.")
        print(">> Is there another file or folder with previous content?")
        sys.exit(1)

    faker_factory = Faker()
    sc = SparkContext()
    spark = SparkSession(sc)
    spark.sparkContext.setLogLevel("FATAL")  # Reduce verbosity of pyspark

    # From this point, it is assumed to have a valid json to generate the data
    dataframe_data = []
    for iteration in range(args.nrows):
        dataframe_data.append(json.dumps(createValuesFromDict(dict_object=json_value, faker_factory=faker_factory)))

    generateDataFrameFromDictionaryList(dataframe_data, destinyPath=args.outputPath, spark=spark, sparkContext=sc)

    sample_folder_path = "/tmp/delete_me"
    sample_file_name = "/result.json"

    if not os.path.exists(sample_folder_path):
        os.makedirs(sample_folder_path)
    with open(sample_folder_path + sample_file_name, "w") as _file:
        _file.write(dataframe_data[0])

    print("End of the program. Check the files in the folder: " + args.outputPath)
    print("A sample of the parquet file (first row) can be visualize from this folder: " + sample_folder_path + sample_file_name)


if __name__ == "__main__":
    main()
