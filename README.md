# Overview
Based on the library faker of python.

It creates parquet files with the schema specified in the json file supplied as parameter.

## Use

To use run the command:

- `python faker_files_generator.py -i CONFIGFILE_PATH -o OUTPUT_PATH`
- i.e.:  `python faker_files_generator.py -i examples/readme_example.json -o /tmp/example_readme`
 
Where CONFIGFILE_PATH is the absolute path of the json file with the fields to be populated.

The files are stored in parquet format but the *first sample can be visualized as json file* (with the browser for instance).

 
## Definition of CONFIG FILE

The file used for the configuration is a json file with the following structure:

    {
        "colName1" : "sentence",
        "colName2" : "date_this_year().isoformat()",
        "colName3" : ["one_not_supported", "pydecimal(left_digits=3, right_digits=2, positive=True)", "name", "four_not_supported"],
        "colName4" : {
          "nestedType1": "year",
          "nestedType2": "email",
          "nestedType3": "name()"
        }
    }

    

The schema is a dictionary which values can be specified with either a:
 -  **value**: value supported by [faker library](https://faker.readthedocs.io/en/stable/providers.html), in case value is **not supported** it will be **used as literal**
    - the use of the last parentesis is optional. Thus, name and name() are equivalent.
 -  **list**: a list of any other value
 -  **another json/dictionary**
  
 All values should be supported by faker library, as they are executed afterwards. The [different commands can be seen in the official documentation](https://faker.readthedocs.io/en/stable/providers.html) (click on the provider to check the command). 
 
## Example   
 An execution with the config file showed above might return:
 
    python faker_files_generator.py -i examples/readme_example.json -o /tmp/readme_example/ -n 3 # -n to specify the number of rows
    pyspark
    spark.read.load("/tmp/readme_example/").collect()
    # It should return something similar(different value) to this rows
    - Row(colName1=u'Professional upon but bad participant interest.', colName2=u'2018-05-09', colName3=[u'one_not_supported', u'444.47', u'Stacy Benitez', u'four_not_supported'], colName4=Row(nestedType1=u'2006', nestedType2=u'turnerdalton@gmail.com', nestedType3=u'Sabrina Vasquez'))
    - Row(colName1=u'Response fly total score relationship air think use.', colName2=u'2018-01-25', colName3=[u'one_not_supported', u'107.36', u'Lindsay Johnson', u'four_not_supported'], colName4=Row(nestedType1=u'1988', nestedType2=u'mcculloughtimothy@yahoo.com', nestedType3=u'Dustin Hickman'))
    - Row(colName1=u'Each fill be against field attorney environment.', colName2=u'2018-06-25', colName3=[u'one_not_supported', u'416.36', u'Samuel Perez', u'four_not_supported'], colName4=Row(nestedType1=u'2015', nestedType2=u'ihoward@hotmail.com', nestedType3=u'Gina Brock'))